CREATE TABLE data
(
  id BIGINT,
  source VARCHAR,
  lookup VARCHAR,
  geodata JSON
);

COPY data FROM '/home/codive/Downloads/programming_check_remote/geodata.csv' DELIMITER ',' CSV HEADER;